---
title: Number 5
description: Mencoba menjadi orang yang sedikit berguna, cukup sedikit saja
date: 2020-01-26T04:15:05+09:00
draft: false
updatesBanner: "Kembali nge-blog... &nbsp; [n5 (nomor 5)](https://n5.gitlab.io) &nbsp; baru saja aktif"
landing:
  height: 500
  image: favicon/android-icon-192x192.png
  title:
    - Nomor 5
  text:
    - Mencoba menjadi orang yang sedikit berguna, sedikit saja.
  titleColor:
  textColor:
  spaceBetweenTitleText: 25
  buttons:
    - link: about
      text: KENALI DIRI
      color: primary
  # backgroundImage: 
  #   src: images/landscape.jpg
  #   height: 600

sections:
  - bgcolor: teal
    type: card
    description: "Blog ini dibuat untuk melatih kemampuan bahasa Indonesia, Inggris, Jepang, dan Jawa. Setiap konten akan diterjemahkan ke dalam bahasa-bahasa tersebut. Namun, untuk konten tertentu yang memiliki tingkat bahasa tinggi, hanya tersedia dalam bahasa Indonesia. Lalu apa saja konten yang ada di blog ini?"
    header: 
      title: Ada Apa di Blog Ini?
      hlcolor: "DarkSlateBlue"
      color: '#fff'
      fontSize: 32
      width: 340
    cards:
      - subtitle: Teknologi
        subtitlePosition: center
        description: "Catatan tentang pengoptimalan perangkat keras dan perangkat lunak yang aku pakai. Agar memudahkan jika ke depan aku butuhkan."
        image: images/section/keyboard.png
        color: white
        button: 
          name: Catatan Teknologi
          link: https://gohugo.io/
          size: large
          target: _blank
          color: 'white'
          bgcolor: '#283593'
      - subtitle: Momen
        subtitlePosition: center
        description: "Tulisan yang bercerita tentang kehidupanku, yang mungkin tidak begitu menarik untuk dibaca. Jadi, tolong tidak usah dibaca."
        image: images/section/processor.png
        color: white
        button: 
          name: Blog
          link: https://gohugo.io/
          size: large
          target: _blank
          color: 'white'
          bgcolor: '#283593'
      - subtitle: Nasehat
        subtitlePosition: center
        description: "Iman itu kadang naik dan kadang turun. Dengan adanya nasehat ini, semoga Allah membimbingku di jalan yang lurus."
        image: images/section/root-server.png
        color: white
        button: 
          name: Nasehat
          link: https://gohugo.io/
          size: large
          target: _blank
          color: 'white'
          bgcolor: '#283593'
  - bgcolor: DarkSlateBlue
    type: normal
    description: "Terima kasih banyak."
    header:
      title: Terima Kasih
      hlcolor: teal
      color: "#fff"
      fontSize: 32
      width: 215
    body:
      subtitle: Atas kebaikannya selama ini.
      subtitlePosition: left
      description: "Terima kasih telah meluangkan waktu untuk berkunjung. <br/> \"Tidak ada balasan kebaikan, kecuali kebaikan.\""
      color: white
      image: images/section/root-server.png
      imagePosition: left

footer:
  sections:
    - title: Proyek Mangkrak
      links:
        - title: Kisah Big O
          link: https://gohugo.io/
        - title: Pohon Silsilah
          link: https://gohugo.io/
    - title: Hobi
      links:
        - title: Pemrograman
          link: https://gohugo.io/
        - title: Desain Grafis
          link: https://gohugo.io/
        - title: Nggak Ngapa-ngapain
          link: https://gohugo.io/
    - title: Media Sosial
      links:
        - title: Facebook
          link: https://gohugo.io/
        - title: Twitter
          link: https://gohugo.io/
        - title: Instagram
          link: https://gohugo.io/
  contents: 
    align: left
    applySinglePageCss: false
    markdown:
      |
      ## Number 5
      Copynight © 2020. All nights reserved.
---

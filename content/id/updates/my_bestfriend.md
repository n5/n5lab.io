---
title: "Teman Terbaik"
description: "Berisi daftar temanku yang paling baik"
date: 2020-01-28T00:10:42+09:00
draft: false
weight: -2
---

Teman-temanku turut berperan dalam perjalananku mencari jati diri. Dan teman-teman terbaik membawaku ke jalan yang terbaik. Aku menganggap mereka adalah teman terbaik, meskipun bagi mereka mungkin aku hanyalah teman biasa karena mereka juga memiliki teman terbaik versi mereka.

### Khanang Eka Prasetya

Teman sebangku, satu kelas, satu SMA, dan juga dalam beberapa organisasi.

### Ahmad Subhan

### Rohmat Basuki

### Muhammad Hablul Barri

### Noreta Agus Sasono

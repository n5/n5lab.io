---
title: "Orang yang Aku Hormati"
description: "Orang di luar keluargaku yang berjasa padaku"
date: 2020-01-28T00:10:37+09:00
draft: false
weight: -3
---

Aku menaruh hormat karena ucapan, perbuatan, sikap, dan kebaikan yang mereka lakukan padaku.

### Pak Margo Pujiantara dan keluarga.

### Mbah Mundir

### Pak Ahmad

### Pak Sarkoni

### Pak Gun Shobirin

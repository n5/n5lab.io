---
title: "Keluargaku"
description: "Tentang ayah, ibu, saudara-saudari, dan kerabat"
date: 2020-01-28T00:10:48+09:00
draft: false
weight: -4
---

Aku dilahirkan dalam keluarga sederhana, menjalani kehidupan sederhana, dan memiliki impian yang sederhana. Aku bersyukur telah lahir di keluarga ini.

### Ayah

### Ibu

### Kakak Pertama

### Kakak Kedua

### Kakak Ketiga

### Kakak keempat (almarhumah)

### Kakak kelima

### Aku

### Adik

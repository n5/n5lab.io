---
title: "Diriku"
description: "Beberapa hal mengenai diriku sendiri, Heru Sularto"
date: 2020-01-28T00:10:51+09:00
draft: false
weight: -5
---

Salam kenal, saudara-saudari. Perkenalkan, namaku Heru Sularto. Biasa dipanggil eR. Pada halaman ini aku akan sedikit menjelaskan tentang diriku. Selamat membaca.

### Fisik
Tinggi badan 182 cm, berat badan 57 kg. Sedikit *underweight*, namun temanku berkata bahwa aku tidak kurus, hanya karena aku tinggi jadi aku terlihat kurus.

### Bakat
Alhamdulillah, Allah menganugerahiku bakat dalam kemampuan logika.

### Kesukaan
Aku suka suasana sepi, hujan di sore hari, angin dingin di pagi hari di bulan November, menulis di blog, bermain dengan anak-anak, bermain futsal dengan teman-teman, berwisata naik sepeda motor, tidur bermalam di tempat umum, hidup apa adanya, 

### Ketidak sukaan
Aku tidak suka mencampuri urusan orang lain, tidak suka mengatur, tidak suka diatur, tidak suka terlibat konflik, tidak suka keributan, tidak suka berdebat, tidak suka kebohongan, tidak suka pelanggar janji,

### Kelebihan
Kata orang-orang atau pun menurut diriku sendiri, aku adalah orang yang ramah, ringan tangan, 

### Kekurangan
Kata orang-orang dan menurut diriku sendiri, aku adalah orang yang tidak bermutu, main game melulu, tidak tegas, 

### Prestasi
Alhamdulillah, ada prestasi yang aku miliki. Aku pernah meraih juara 3 murid teladan tingkat sekolah dasar (SD) di Kecamatan Kedunggalar.

### Impian
Aku memiliki impian-impian. Namun seiring berjalannya waktu, terkadang impian-impian tersebut sepertinya mustahil untuk aku raih, lalu aku pun menguburnya. Dan sebagian impian berganti dengan impian-impian lain yang lebih sederhana dan kemungkinan dapat terwujud. Impianku saat ini adalah mewujudkan impian orang tua, bekerja, menikah dan memiliki anak-anak laki-laki, membangun lapangan kerja dan memberdayakan masyarakat di kampung.

### Ketakutan
Tidak banyak hal yang aku takutkan. Namun, terkadang muncul ketakutan tanpa diduga. Salah satu hal yang aku takutkan yang ada di dalam namaku (Heru S**ular**to), adalah ular. Terkadang aku takut digigit ular. Dulu saat kelas tiga SD, aku pernah digigit ular berbisa (*Trimesurus albolabris*) hingga harus dirawat di rumah sakit selama satu pekan.

### Kesedihan
Hal-hal yang membuat aku sedih adalah kegagalanku, penderitaan orang-orang tercinta, kebaikan orang lain. Tidak banyak yang tahu tentang masa laluku. Aku sering tersenyum dan orang-orang mengira aku baik-baik saja. Aku mengira diriku mengalami gejala bipolar disorder atau pun hipophrenia. Namun sepertinya aku hanya terlalu jauh dari Allah.

### Amarah
Aku berusaha menahan marah. Aku sering bersedih, namun jarang marah. Seperti yang dikatakan oleh seseorang, bahwa emosi yang tersalurkan akan menghasilkan amarah, sedangkan emosi yang tidak tersalurkan akan menghasilkan kesedihan.

### 
